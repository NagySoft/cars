package com.cars.presentation.presenter.impl;

import android.util.Log;

import com.cars.domain.controllers.Controller;
import com.cars.domain.interactors.ResponseCallback;
import com.cars.domain.interactors.cars.MainClassInteractorImpl;
import com.cars.presentation.presenter.MainPresenter;

import retrofit2.Retrofit;

/**
 * Created by ahmed on 03/07/16.
 */
public class MainClassPresenterImpl implements MainPresenter, ResponseCallback.CallbackStates {
    private static final String TAG = MainClassPresenterImpl.class.getSimpleName();
    MainPresenter.PresenterCallback presenterCallback;
    ResponseCallback responseCallback;

    public MainClassPresenterImpl(Retrofit retrofit, PresenterCallback presenterCallback) {
        this.presenterCallback = presenterCallback;
        Controller.GetCars getCars = retrofit.create(Controller.GetCars.class);
        responseCallback = new MainClassInteractorImpl(getCars, this);
    }

    @Override
    public void success(Object data) {
        Log.i(TAG, "success: " + data);
        presenterCallback.updateView(data);
    }

    @Override
    public void failure(Throwable throwable) {
        Log.e(TAG, "failure: ", throwable);
        presenterCallback.showError(throwable);
    }

    @Override
    public void resumed() {
    }

    @Override
    public void paused() {
    }

    @Override
    public void stopped() {
    }

    @Override
    public void destroyed() {
        responseCallback.unsubscribe();
    }

}