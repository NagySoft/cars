package com.cars.presentation.presenter;

/**
 * Created by ahmed on 23/06/16.
 */
public interface MainPresenter extends LifeCycle {
    interface PresenterCallback {
        void updateView(Object data);

        boolean showError(Throwable throwable);

        void showLoading(boolean show);
    }
}
