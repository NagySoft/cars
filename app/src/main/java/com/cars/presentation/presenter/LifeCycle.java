package com.cars.presentation.presenter;

/**
 * Created by ahmed on 23/06/16.
 */
public interface LifeCycle {
    void resumed();

    void paused();

    void stopped();

    void destroyed();
}
