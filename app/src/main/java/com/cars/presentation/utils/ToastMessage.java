package com.cars.presentation.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by ahmed on 21/05/16.
 */
public class ToastMessage {
    public static void message(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
