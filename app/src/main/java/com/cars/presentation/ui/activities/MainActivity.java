package com.cars.presentation.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.cars.R;
import com.cars.presentation.ui.fragments.CarsListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        goToMainClass();
    }

    private void goToMainClass() {
        CarsListFragment fragment = CarsListFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
