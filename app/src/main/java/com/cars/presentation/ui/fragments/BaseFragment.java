package com.cars.presentation.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.cars.CarsApplication;
import com.cars.presentation.presenter.MainPresenter;
import com.cars.presentation.utils.ToastMessage;

import javax.inject.Inject;

import retrofit2.Retrofit;

/**
 * Created by Ahmed on 15-Jul-16.
 */
public abstract class BaseFragment
        extends Fragment
        implements MainPresenter.PresenterCallback {
    private static final String TAG = BaseFragment.class.getSimpleName();
    protected Context activity;
    @Inject
    protected SharedPreferences preferences;
    @Inject
    protected Retrofit retrofit;
    protected MainPresenter presenter;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        CarsApplication app = (CarsApplication) getActivity().getApplication();
        app.getAppComponent().inject(this);
    }

    @Override
    public boolean showError(Throwable throwable) {
        ToastMessage.message(getActivity(), throwable.getMessage());
        return false;
    }

    @Override
    public void onDestroy() {
        if (presenter != null)
            presenter.destroyed();
        super.onDestroy();
    }
}
