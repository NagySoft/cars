package com.cars.presentation.ui.adapters;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.cars.R;
import com.cars.domain.controllers.listners.LsnrAction;
import com.cars.domain.models.cars.AuctionInfo;
import com.cars.domain.models.cars.Cars;
import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

/**
 * Created by Ahmed on 13-Jul-16.
 */
public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {
    public static final int s = 9;
    private List<Cars> items;

    private RecyclerView recyclerView;

    public AdapterRecyclerView(List<Cars> items ) {
        this.items = items;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        if (items.get(0) != null) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_cars, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Cars cars = new Cars();
        Object object = items.get(position);
        cars.setModelEn((String) ((LinkedTreeMap) object).get("modelEn"));
        cars.setMakeEn((String) ((LinkedTreeMap) object).get("makeEn"));
        cars.setImage((String) ((LinkedTreeMap) object).get("image"));
        cars.setYear((double) ((LinkedTreeMap) object).get("year"));
        AuctionInfo auctionInfo = new AuctionInfo();
        Object object2 = ((LinkedTreeMap) object).get("AuctionInfo");
        auctionInfo.setCurrencyEn((String) ((LinkedTreeMap) object2).get("currencyEn"));
        auctionInfo.setCurrentPrice((double) ((LinkedTreeMap) object2).get("currentPrice"));
        auctionInfo.setLot((double) ((LinkedTreeMap) object2).get("lot"));
        auctionInfo.setBids((double) ((LinkedTreeMap) object2).get("bids"));

        if (holder.img != null) {
            if (cars.getImage() != null
                    && !cars.getImage().isEmpty()
                    && recyclerView != null) {
                if (recyclerView != null)
                    if (recyclerView.getContext() != null)
                        if (cars.getImage() != null)
                            if (!cars.getImage().equals("")) {
                                GlideUrl glideUrl =
                                        new GlideUrl(cars.getImage());
                                Glide.with(recyclerView.getContext()).load(glideUrl).asBitmap()
                                        .centerCrop()
                                        .placeholder(R.drawable.loading_spinner)
                                        .into(holder.img);
                            }
            } else {
                Glide.with(recyclerView.getContext())
                        .load(R.drawable.profile_default)
                        .asBitmap()
                        .centerCrop()
                        .placeholder(R.drawable.loading_spinner)
                        .into(new BitmapImageViewTarget(holder.img) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(recyclerView.getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.img.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            }
        }
        holder.tvTitle.setText(cars.getModelEn() + " " + cars.getMakeEn() + " " + cars.getYear());
        holder.tvPrice.setText(auctionInfo.getCurrentPrice() + " " + auctionInfo.getCurrencyEn());
        holder.tvLots.setText(auctionInfo.getLot() + "");
        holder.tvBids.setText(auctionInfo.getBids() + "");
        holder.tvTimeLeft.setText("");

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTitle;
        TextView tvPrice;
        TextView tvLots;
        TextView tvBids;
        TextView tvTimeLeft;
        LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            this.img = view.findViewById(R.id.image);
            this.tvTitle = view.findViewById(R.id.tv_title);
            this.tvPrice = view.findViewById(R.id.tv_price);
            this.tvLots = view.findViewById(R.id.tv_lots);
            this.tvBids = view.findViewById(R.id.tv_bids);
            this.tvTimeLeft = view.findViewById(R.id.tv_timeleft);
        }
    }
}
