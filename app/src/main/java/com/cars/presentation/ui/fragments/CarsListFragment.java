package com.cars.presentation.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cars.CarsApplication;
import com.cars.R;
import com.cars.domain.models.cars.Cars;
import com.cars.domain.models.cars.MainClass;
import com.cars.presentation.presenter.MainPresenter;
import com.cars.presentation.presenter.impl.MainClassPresenterImpl;
import com.cars.presentation.ui.adapters.AdapterRecyclerView;
import com.cars.presentation.utils.ToastMessage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class CarsListFragment extends BaseFragment {
    private static final String TAG = CarsListFragment.class.getSimpleName();
    @Inject
    protected SharedPreferences preferences;
    @Inject
    protected Retrofit retrofit;
    protected MainPresenter presenter;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    AdapterRecyclerView adaptor;
    Context activity;
    List<Cars> cars;

    public CarsListFragment() {
        // Required empty public constructor
    }

    public static CarsListFragment newInstance() {
        CarsListFragment fragment = new CarsListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        ((CarsApplication) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cars_list, container, false);
        ButterKnife.bind(this, rootView);
        if (activity != null) {
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
            recyclerView.setLayoutManager(layoutManager);
            cars = new ArrayList<>();
            adaptor = new AdapterRecyclerView(cars);
            recyclerView.setAdapter(adaptor);
            refreshLayout.setColorSchemeResources(R.color.refreshColor1, R.color.refreshColor2,
                    R.color.refreshColor3, R.color.refreshColor4);
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getData();
                }
            });
            getData();
        }
        return rootView;
    }

    private void getData() {
        refreshLayout.setRefreshing(true);
        presenter = new MainClassPresenterImpl(retrofit, this);
    }

    @Override
    public void updateView(Object data) {
        refreshLayout.setRefreshing(false);
        cars.clear();
        cars.addAll(((MainClass) data).getCars());
        adaptor.notifyDataSetChanged();
        recyclerView.setAdapter(adaptor);
    }

    @Override
    public boolean showError(Throwable throwable) {
        refreshLayout.setRefreshing(false);
        boolean displyed = super.showError(throwable);
        if (!displyed) {
            ToastMessage.message(getActivity(), getString(R.string.error_while_loading_data));
        }
        return true;
    }

    @Override
    public void showLoading(boolean show) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.destroyed();
        getActivity().finish();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Log.i(TAG, "onHiddenChanged: ");
        super.onHiddenChanged(hidden);
    }

}
