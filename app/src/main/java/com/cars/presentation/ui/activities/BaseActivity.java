package com.cars.presentation.ui.activities;

/**
 * Created by ahmed on 03/07/16.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;

import com.cars.CarsApplication;
import com.cars.R;
import com.cars.presentation.presenter.MainPresenter;
import com.cars.presentation.utils.ToastMessage;

import javax.inject.Inject;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;

public abstract class BaseActivity
        extends AppCompatActivity
        implements MainPresenter.PresenterCallback {
    protected static final String TAG = BaseActivity.class.getSimpleName();
    @Inject
    protected Retrofit retrofit;
    protected MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((CarsApplication) getApplication()).getAppComponent().inject(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public void updateView(Object data) {
        Log.i(TAG, "updateView: " + data);
    }

    @Override
    public boolean showError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            ToastMessage.message(this, exception.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public void showLoading(boolean show) {

    }

    @Override
    public void onDestroy() {
        if (presenter != null)
            presenter.destroyed();
        super.onDestroy();
    }
}

