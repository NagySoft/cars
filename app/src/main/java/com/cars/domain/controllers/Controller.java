package com.cars.domain.controllers;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ahmed on 03/07/16.
 */
public interface Controller {
    interface GetCars {
        @GET("carsonline")
        Observable<Object> getCars();
    }
}
