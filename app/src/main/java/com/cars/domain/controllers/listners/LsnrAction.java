package com.cars.domain.controllers.listners;

import com.cars.domain.models.cars.Cars;

/**
 * Created by Ahmed on 15-Jul-16.
 */
public interface LsnrAction {
    public void perform(Cars item);
}
