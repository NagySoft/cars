package com.cars.domain.interactors;

/**
 * Created by ahmed on 23/06/16.
 */
public interface ResponseCallback {
    void unsubscribe();

    interface CallbackStates {
        void success(Object data);

        void failure(Throwable throwable);
    }
}
