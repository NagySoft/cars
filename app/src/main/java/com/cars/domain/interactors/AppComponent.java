package com.cars.domain.interactors;

import com.cars.domain.modules.ApiModule;
import com.cars.domain.modules.AppModule;
import com.cars.domain.modules.PrefrencesModule;
import com.cars.presentation.ui.activities.BaseActivity;
import com.cars.presentation.ui.fragments.CarsListFragment;
import com.cars.presentation.ui.fragments.BaseFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ahmed on 23/06/16.
 */
@Singleton
@Component(modules = {AppModule.class, ApiModule.class, PrefrencesModule.class})
public interface AppComponent {
    void inject(BaseActivity baseActivity);

    void inject(CarsListFragment MainClassFragment);

    void inject(BaseFragment baseFragment);
}
