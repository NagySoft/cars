package com.cars.domain.interactors.cars;

import android.util.Log;

import com.cars.domain.controllers.Controller;
import com.cars.domain.interactors.ResponseCallback;
import com.cars.domain.interactors.base.UseCase;
import com.cars.domain.models.cars.Cars;
import com.cars.domain.models.cars.MainClass;
import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by ahmed on 03/07/16.
 */
public class MainClassInteractorImpl extends UseCase implements ResponseCallback {
    private static final String TAG = MainClassInteractorImpl.class.getSimpleName();
    CallbackStates state;
    Controller.GetCars getCars;

    public MainClassInteractorImpl(Controller.GetCars getCars, CallbackStates state) {
        this.getCars = getCars;
        this.state = state;
        this.execute(new MainClassSubscriber());
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.getCars.getCars();
    }

    private final class MainClassSubscriber extends Subscriber<Object> {

        @Override
        public void onCompleted() {
            Log.i(TAG, "onCompleted: ");

        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
            Log.e("Error", "Network " + throwable.getCause());
            state.failure(throwable);
        }

        @Override
        public void onNext(Object data) {
            Log.i(TAG, "onNext: " + data);
            MainClass mainClass = new MainClass();
            mainClass.setSortOption((String) ((LinkedTreeMap) data).get("sortOption"));
            mainClass.setCars((List<Cars>) ((LinkedTreeMap) data).get("Cars"));
            mainClass.setCount((double) ((LinkedTreeMap) data).get("count"));
            mainClass.setAlertAr((String) ((LinkedTreeMap) data).get("alertAr"));
            mainClass.setEndDate((double) ((LinkedTreeMap) data).get("endDate"));
            mainClass.setAlertEn((String) ((LinkedTreeMap) data).get("alertEn"));
            mainClass.setSortDirection((String) ((LinkedTreeMap) data).get("sortDirection"));
            mainClass.setTicks((String) ((LinkedTreeMap) data).get("Ticks"));
            mainClass.setRefreshInterval((double) ((LinkedTreeMap) data).get("RefreshInterval"));
//            mainClass.setError(((LinkedTreeMap) data).get("Error"));
            state.success(mainClass);
        }
    }
}
