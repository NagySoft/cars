package com.cars.domain.models.cars;

import android.os.Parcel;
import android.os.Parcelable;

public class AuctionInfo implements Parcelable {
    private String iVinNumber;

    private String endDateAr;

    private String isModified;

    private String endDateEn;

    private String minIncrement;

    private double bids;

    private String endDate;

    private String currencyEn;

    private double currentPrice;

    private String itemid;

    private String VATPercent;

    private String priority;

    private String iCarId;

    private String currencyAr;

    private double lot;

    public String getIVinNumber() {
        return iVinNumber;
    }

    public void setIVinNumber(String iVinNumber) {
        this.iVinNumber = iVinNumber;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getIsModified() {
        return isModified;
    }

    public void setIsModified(String isModified) {
        this.isModified = isModified;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getMinIncrement() {
        return minIncrement;
    }

    public void setMinIncrement(String minIncrement) {
        this.minIncrement = minIncrement;
    }

    public double getBids() {
        return bids;
    }

    public void setBids(double bids) {
        this.bids = bids;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getVATPercent() {
        return VATPercent;
    }

    public void setVATPercent(String VATPercent) {
        this.VATPercent = VATPercent;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getICarId() {
        return iCarId;
    }

    public void setICarId(String iCarId) {
        this.iCarId = iCarId;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public double getLot() {
        return lot;
    }

    public void setLot(double lot) {
        this.lot = lot;
    }

    @Override
    public String toString() {
        return "ClassPojo [iVinNumber = " + iVinNumber + ", endDateAr = " + endDateAr + ", isModified = " + isModified + ", endDateEn = " + endDateEn + ", minIncrement = " + minIncrement + ", bids = " + bids + ", endDate = " + endDate + ", currencyEn = " + currencyEn + ", currentPrice = " + currentPrice + ", itemid = " + itemid + ", VATPercent = " + VATPercent + ", priority = " + priority + ", iCarId = " + iCarId + ", currencyAr = " + currencyAr + ", lot = " + lot + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.iVinNumber);
        dest.writeString(this.endDateAr);
        dest.writeString(this.isModified);
        dest.writeString(this.endDateEn);
        dest.writeString(this.minIncrement);
        dest.writeDouble(this.bids);
        dest.writeString(this.endDate);
        dest.writeString(this.currencyEn);
        dest.writeDouble(this.currentPrice);
        dest.writeString(this.itemid);
        dest.writeString(this.VATPercent);
        dest.writeString(this.priority);
        dest.writeString(this.iCarId);
        dest.writeString(this.currencyAr);
        dest.writeDouble(this.lot);
    }

    public AuctionInfo() {
    }

    protected AuctionInfo(Parcel in) {
        this.iVinNumber = in.readString();
        this.endDateAr = in.readString();
        this.isModified = in.readString();
        this.endDateEn = in.readString();
        this.minIncrement = in.readString();
        this.bids = in.readDouble();
        this.endDate = in.readString();
        this.currencyEn = in.readString();
        this.currentPrice = in.readDouble();
        this.itemid = in.readString();
        this.VATPercent = in.readString();
        this.priority = in.readString();
        this.iCarId = in.readString();
        this.currencyAr = in.readString();
        this.lot = in.readDouble();
    }

    public static final Parcelable.Creator<AuctionInfo> CREATOR = new Parcelable.Creator<AuctionInfo>() {
        @Override
        public AuctionInfo createFromParcel(Parcel source) {
            return new AuctionInfo(source);
        }

        @Override
        public AuctionInfo[] newArray(int size) {
            return new AuctionInfo[size];
        }
    };
}
