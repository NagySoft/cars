package com.cars.domain.models.cars;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Ahmed on 22-Jul-17.
 */

public class MainClass implements Parcelable {

    private String sortOption;

    private List<Cars> Cars;

    private double count;

    private String alertAr;

    private double endDate;

    private String alertEn;

    private String sortDirection;

    private String Ticks;

    private double RefreshInterval;

//    private LinkedTreeMap Error;

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public List<Cars> getCars() {
        return Cars;
    }

    public void setCars(List<Cars> Cars) {
        this.Cars = Cars;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public void setAlertAr(String alertAr) {
        this.alertAr = alertAr;
    }

    public double getEndDate() {
        return endDate;
    }

    public void setEndDate(double endDate) {
        this.endDate = endDate;
    }

    public String getAlertEn() {
        return alertEn;
    }

    public void setAlertEn(String alertEn) {
        this.alertEn = alertEn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getTicks() {
        return Ticks;
    }

    public void setTicks(String Ticks) {
        this.Ticks = Ticks;
    }

    public double getRefreshInterval() {
        return RefreshInterval;
    }

    public void setRefreshInterval(double RefreshInterval) {
        this.RefreshInterval = RefreshInterval;
    }

//    public LinkedTreeMap getError() {
//        return Error;
//    }

//    public void setError(LinkedTreeMap Error) {
//        this.Error = Error;
//    }


    @Override
    public String toString() {
        return "MainClass{" +
                "sortOption='" + sortOption + '\'' +
                ", Cars=" + Cars +
                ", count=" + count +
                ", alertAr='" + alertAr + '\'' +
                ", endDate=" + endDate +
                ", alertEn='" + alertEn + '\'' +
                ", sortDirection='" + sortDirection + '\'' +
                ", Ticks='" + Ticks + '\'' +
                ", RefreshInterval=" + RefreshInterval +
                '}';
    }

    public MainClass() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sortOption);
        dest.writeTypedList(this.Cars);
        dest.writeDouble(this.count);
        dest.writeString(this.alertAr);
        dest.writeDouble(this.endDate);
        dest.writeString(this.alertEn);
        dest.writeString(this.sortDirection);
        dest.writeString(this.Ticks);
        dest.writeDouble(this.RefreshInterval);
    }

    protected MainClass(Parcel in) {
        this.sortOption = in.readString();
        this.Cars = in.createTypedArrayList(com.cars.domain.models.cars.Cars.CREATOR);
        this.count = in.readDouble();
        this.alertAr = in.readString();
        this.endDate = in.readDouble();
        this.alertEn = in.readString();
        this.sortDirection = in.readString();
        this.Ticks = in.readString();
        this.RefreshInterval = in.readDouble();
    }

    public static final Creator<MainClass> CREATOR = new Creator<MainClass>() {
        @Override
        public MainClass createFromParcel(Parcel source) {
            return new MainClass(source);
        }

        @Override
        public MainClass[] newArray(int size) {
            return new MainClass[size];
        }
    };
}
