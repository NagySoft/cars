package com.cars.domain.models.cars;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ahmed on 22-Jul-17.
 */

public class Cars implements Parcelable {
    private String sharingLink;
    private String mileage;
    private String sharingMsgEn;
    private String bodyEn;
    private String descriptionEn;
    private String descriptionAr;
    private String image;
    private String bodyId;
    private String makeEn;
    private AuctionInfo AuctionInfo;
    private String modelID;
    private String carID;
    private String sharingMsgAr;
    private String bodyAr;
    private String modelEn;
    private String modelAr;
    private double year;
    private String makeID;
    private String imgCount;
    private String makeAr;

    public String getSharingLink() {
        return sharingLink;
    }

    public void setSharingLink(String sharingLink) {
        this.sharingLink = sharingLink;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getSharingMsgEn() {
        return sharingMsgEn;
    }

    public void setSharingMsgEn(String sharingMsgEn) {
        this.sharingMsgEn = sharingMsgEn;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        image = image.replace("[w]", "100");
        image = image.replace("[h]", "100");
        this.image = image;
    }

    public String getBodyId() {
        return bodyId;
    }

    public void setBodyId(String bodyId) {
        this.bodyId = bodyId;
    }

    public String getMakeEn() {
        return makeEn;
    }

    public void setMakeEn(String makeEn) {
        this.makeEn = makeEn;
    }

    public AuctionInfo getAuctionInfo() {
        return AuctionInfo;
    }

    public void setAuctionInfo(AuctionInfo AuctionInfo) {
        this.AuctionInfo = AuctionInfo;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public String getSharingMsgAr() {
        return sharingMsgAr;
    }

    public void setSharingMsgAr(String sharingMsgAr) {
        this.sharingMsgAr = sharingMsgAr;
    }

    public String getBodyAr() {
        return bodyAr;
    }

    public void setBodyAr(String bodyAr) {
        this.bodyAr = bodyAr;
    }

    public String getModelEn() {
        return modelEn;
    }

    public void setModelEn(String modelEn) {
        this.modelEn = modelEn;
    }

    public String getModelAr() {
        return modelAr;
    }

    public void setModelAr(String modelAr) {
        this.modelAr = modelAr;
    }

    public double getYear() {
        return year;
    }

    public void setYear(double year) {
        this.year = year;
    }

    public String getMakeID() {
        return makeID;
    }

    public void setMakeID(String makeID) {
        this.makeID = makeID;
    }

    public String getImgCount() {
        return imgCount;
    }

    public void setImgCount(String imgCount) {
        this.imgCount = imgCount;
    }

    public String getMakeAr() {
        return makeAr;
    }

    public void setMakeAr(String makeAr) {
        this.makeAr = makeAr;
    }

    @Override
    public String toString() {
        return "ClassPojo [sharingLink = " + sharingLink + ", mileage = " + mileage + ", sharingMsgEn = " + sharingMsgEn + ", bodyEn = " + bodyEn + ", descriptionEn = " + descriptionEn + ", descriptionAr = " + descriptionAr + ", image = " + image + ", bodyId = " + bodyId + ", makeEn = " + makeEn + ", AuctionInfo = " + AuctionInfo + ", modelID = " + modelID + ", carID = " + carID + ", sharingMsgAr = " + sharingMsgAr + ", bodyAr = " + bodyAr + ", modelEn = " + modelEn + ", modelAr = " + modelAr + ", year = " + year + ", makeID = " + makeID + ", imgCount = " + imgCount + ", makeAr = " + makeAr + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sharingLink);
        dest.writeString(this.mileage);
        dest.writeString(this.sharingMsgEn);
        dest.writeString(this.bodyEn);
        dest.writeString(this.descriptionEn);
        dest.writeString(this.descriptionAr);
        dest.writeString(this.image);
        dest.writeString(this.bodyId);
        dest.writeString(this.makeEn);
        dest.writeParcelable(this.AuctionInfo, flags);
        dest.writeString(this.modelID);
        dest.writeString(this.carID);
        dest.writeString(this.sharingMsgAr);
        dest.writeString(this.bodyAr);
        dest.writeString(this.modelEn);
        dest.writeString(this.modelAr);
        dest.writeDouble(this.year);
        dest.writeString(this.makeID);
        dest.writeString(this.imgCount);
        dest.writeString(this.makeAr);
    }

    public Cars() {
    }

    protected Cars(Parcel in) {
        this.sharingLink = in.readString();
        this.mileage = in.readString();
        this.sharingMsgEn = in.readString();
        this.bodyEn = in.readString();
        this.descriptionEn = in.readString();
        this.descriptionAr = in.readString();
        this.image = in.readString();
        this.bodyId = in.readString();
        this.makeEn = in.readString();
        this.AuctionInfo = in.readParcelable(com.cars.domain.models.cars.AuctionInfo.class.getClassLoader());
        this.modelID = in.readString();
        this.carID = in.readString();
        this.sharingMsgAr = in.readString();
        this.bodyAr = in.readString();
        this.modelEn = in.readString();
        this.modelAr = in.readString();
        this.year = in.readDouble();
        this.makeID = in.readString();
        this.imgCount = in.readString();
        this.makeAr = in.readString();
    }

    public static final Parcelable.Creator<Cars> CREATOR = new Parcelable.Creator<Cars>() {
        @Override
        public Cars createFromParcel(Parcel source) {
            return new Cars(source);
        }

        @Override
        public Cars[] newArray(int size) {
            return new Cars[size];
        }
    };
}
