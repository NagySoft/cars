package com.cars;

import android.app.Application;

import com.cars.domain.interactors.AppComponent;
import com.cars.domain.interactors.DaggerAppComponent;
import com.cars.domain.modules.ApiModule;
import com.cars.domain.modules.AppModule;
import com.cars.domain.modules.PrefrencesModule;
import com.crashlytics.android.Crashlytics;

import java.io.File;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ahmed on 03/07/16.
 */
public class CarsApplication extends Application {
    static AppComponent appComponent;
    static File cacheFile;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        cacheFile = new File(getCacheDir(), "responses");

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule(cacheFile, this, BuildConfig.CLIENT_INFO_URL))
                .prefrencesModule(new PrefrencesModule())
                .build();
    }

    public void buildDagger(String url) {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule(cacheFile, this, url))
                .prefrencesModule(new PrefrencesModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}